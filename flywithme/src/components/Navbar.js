import React from 'react'
import { Link } from 'react-router-dom'

function Navbar() {
    const navigationstyles={
        backgroundColor:'black',
        color:'white',
        margin:'0px',
        display:'flex',
        flexWrap:'wrap',
        padding:'10px',
        display:'flex',
        flexWrap:'wrap',
        justifyContent:'space-between'
    }
    const links={
        listStyleType:'none',
        cursor:'pointer',
        margin:'0 10px'
    }
    return (
      
        <div style={{position:'static',top:'0px',width:'100%'}}>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
  <div className="container-fluid">
    <h1>Panchhi Bano</h1>
    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarNav">
      <ul className="navbar-nav" style={{marginLeft:'auto'}}>
        <li className="nav-item">
         
          <Link to='/' className='nav-link active'>Home Page</Link>
        </li>
        <li className="nav-item">
         
          <Link to='/about' className='nav-link '>About us</Link>
        </li>
        <li className="nav-item">
         
          <Link to='/contact' className='nav-link '>Contact Us</Link>
        </li>
        <li className="nav-item">
         
          <Link to='/register' className='nav-link '>Register</Link>
        </li>
        <li className="nav-item">
         
          <Link to='/login' className='nav-link '>Login</Link>
        </li>
      </ul>
    </div>
  </div>
</nav>
</div>
    )
}

export default Navbar
