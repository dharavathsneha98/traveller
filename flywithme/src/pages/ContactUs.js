import React from 'react';
import { Container, Row, Col, Form, FormGroup, Label, Input, Button } from 'reactstrap';
import './contact.css'; 

const ContactUsPage = () => {
  const handleSubmit = (e) => {
    e.preventDefault();
   
  };
  return(
  <div className="content">
          <h3>Get in Touch</h3>
          <p>Ready to embark on your next adventure? Contact us today to start planning your dream getaway with Panchibano!</p>
        
    <Container className="contact-container">
      <Row>
        <Col md={{ size: 6, offset: 3 }}>
          <h2>Contact Us</h2>
          <Form onSubmit={handleSubmit}>
            <FormGroup>
              <Label for="name">Name</Label>
              <Input type="text" name="name" id="name" placeholder="Enter your name" required />
            </FormGroup>
            <FormGroup>
              <Label for="email">Email</Label>
              <Input type="email" name="email" id="email" placeholder="Enter your email" required />
            </FormGroup>
            <FormGroup>
              <Label for="message">Message</Label>
              <Input type="textarea" name="message" id="message" placeholder="Enter your message" rows="5" required />
            </FormGroup>
            <Button color="primary" type="submit">Submit</Button>
          </Form>
        </Col>
      </Row>
    </Container>
    </div>
  );
};

export default ContactUsPage;
